# yii2-event-sourcing

Was ist Event-Sourcing:
siehe https://www.heise.de/blog/Grundlagen-von-Event-Sourcing-4998224.html

Dieses Paket wird sich wahrscheinlich nicht an die reine Lehre halten.


![](docs/diagrams/out/active-record-saving.png)

## Installation

Die Installation erfolgt über Composer.
```bash
composer require jki/yii2-event-sourcing
```

## Konfiguration

### Gii-Config
Mit der Config nutzt gii automatisch die ActiveRecord-Klasse aus diesem Paket.
 
config/(web|console).php:
```php
<?php
return [
    'id' => 'app',
    'components' = [],
    'modules' => [
        'gii' => [
            'class' => \yii\gii\Module::class,
            'allowedIPs' => ['127.0.0.1', '::1'],
            'generators' => [
                'models' => \jki\es\gii\ConfigHelper::model()
            ]
        ],
    ],
];
```

## Usage

### Migration

Bei Migrationen mit Datenänderungen an bestehenden Tabellen mit Event-Sourcing ActiveRecord-Klassen, darf nicht die standard
Yii `yii\db\Migration` Klasse verwendet werden, sondern die Klasse `jki\es\componets\Migration`. 
Diese Klasse erweitert die `yii\db\Migration` und fügt die Methoden `setActiveEsRecord()` und `disableEsRecord()` hinzu.
Diese Methoden müssen vor und nach der `insert()` bzw `update()` Methoden aufgerufen werden, denn so werden die
Event-Store-Tabelle bzw Snapshot-Tabelle mit Daten gefüllt. Denn nur so kann eine Versionierung der Daten erfolgen.

```php
<?php

namespace app\migrations;

use jki\es\components\Migration;

class BeispielMigration extends Migration
{
    public function safeUp()
    {
        $this->setActiveEsRecord('app\\models\\Example');
        $this->insert(
            'example', 
            [
                'name' => 'Maxi Musterfrau', 
                'email' => 'maxi.musterfrau@example.com',
                'strasse' => 'Musterstraße 2', 
                'plz' => '12345', 
                'ort' => 'Musterstadt'
            ]
        );
        $this->upsert(
            'example', 
            [
                'plz' => '54321', 
                'ort' => 'Musterdorf'
            ], 
            [ 
                'email' => 'maxi.musterfrau@example.com'
            ]
        );
    }

}
```

