<?php

namespace jki\es\models;


use DateTime;
use jki\es\events\DefaultEvent;
use jki\es\events\EventSourceEvent;
use Yii;
use yii\base\NotSupportedException;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;

/**
 * @property-read int $versionNumber
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    const EVENT_AFTER_REPLAY = 'afterReplay';

    public int $snapshotInterval = 25;

    protected ?Transaction $transaction = null;

    /**
     *
     * @var array|string[]
     */
    protected array $eventList
        = [
            self::SCENARIO_DEFAULT => DefaultEvent::class,
        ];

    /**
     * @inheritdocs
     * @param array<string|int, string|array<string|int,string>> $config
     */
    public final function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function beforeSave($insert)
    {

        if (!ArrayHelper::keyExists($this->getScenario(), $this->eventList)) {
            Yii::error('Scenario not found in event-list, please add it to the event-list before saving');
            return false;
        }

        $this->transaction = Yii::$app->db->beginTransaction();

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdocs
     * @param array<int|string, string> $attributeNames
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $result = parent::save($runValidation, $attributeNames);
        if (!$result && !empty($this->transaction)) {
            // @codeCoverageIgnoreStart
            $this->transaction->rollBack();
            // @codeCoverageIgnoreEnd
        }

        return $result;
    }

    /**
     * @inheritdoc
     * @param array<string, string|int> $changedAttributes
     * @return void
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (empty($changedAttributes)) {
            $this->transaction->commit();
            return;
        }

        if ($this->eventList[$this->getScenario()]::onSave($this, $insert, $changedAttributes)) {
            if (!is_null($this->transaction)) {
                $this->transaction->commit();
            }
            return;
        }
        // @codeCoverageIgnoreStart
        if (!is_null($this->transaction)) {
            $this->transaction->rollBack();
        }
        // @codeCoverageIgnoreEnd
    }

    public function createSnapshot(?int $version = null): bool
    {
        if ($version == null) {
            $version = EventStore::find()
                                 ->where(['model_class' => static::class, 'model_id' => $this->getPrimaryKey()])
                                 ->max('model_version');
        }

        $snapshot                = new Snapshot();
        $snapshot->model_class   = static::class;
        $snapshot->model_id      = $this->getPrimaryKey();
        $snapshot->model_version = $version;
        $snapshot->data          = serialize($this->attributes);

        return $snapshot->save();
    }

    /**
     * returns a ActiveRecord object in the state of the given version
     */
    public static function getVersion(int $id, ?int $version): static
    {
        if (is_null($version)) {
            return new static();
        }

        /** @var Snapshot|null $snapshot */
        $snapshot        = Snapshot::find()
                                   ->andWhere(['model_class' => static::class, 'model_id' => $id])
                                   ->andWhere(['<=', 'model_version', $version])
                                   ->orderBy(['model_version' => SORT_DESC])
                                   ->one();
        $snapshotVersion = $snapshot->model_version ?? 0;
        $model           = null;
        if (!is_null($snapshot)) {
            $snapshotVersion = $snapshot->model_version;
            $model           = new static();
            $model->setAttributes(unserialize($snapshot->data ?? ''));
        } else {
            $model = new static();
        }

        $model->id          = $id;
        $model->isNewRecord = false;

        /** @var EventStore[] $events */
        $events = EventStore::find()
                            ->where(['model_class' => static::class, 'model_id' => $id])
                            ->andWhere(['>', 'model_version', $snapshotVersion])
                            ->andWhere(['<=', 'model_version', $version])
                            ->orderBy(['model_version' => SORT_ASC])
                            ->all();

        foreach ($events as $event) {
            $klasseNameKurz = $model->formName();
            if (in_array($event->event, ["Create{$klasseNameKurz}", "Update{$klasseNameKurz}"])) {
                if (is_array($event->payload)) {
                    DefaultEvent::onReplay($model, $event->payload);
                }
            } else {
                /** @var EventSourceEvent $tmp */
                $tmp = $event->event;
                if (is_array($event->payload)) {
                    $tmp::onReplay($model, $event->payload);
                }
            }
        }

        $model->afterReplay();

        return $model;
    }

    public function afterReplay():void
    {
        $this->trigger(static::EVENT_AFTER_REPLAY);
    }

    public static function getVersionAtTime(int $id, int|string|DateTime $date): static
    {
        $dateO = null;
        if (is_int($date)) {
            $dateO = new DateTime();
            $dateO->setTimestamp($date);
        } elseif (is_string($date)) {
            $dateO = new DateTime($date);
        } elseif ($date instanceof DateTime) {
            $dateO = $date;
        }

        /** @var EventStore $eventStore */
        $eventStore = EventStore::find()
                                ->where(['model_class' => static::class, 'model_id' => $id])
                                ->andWhere(['<=', 'created_at', $dateO->format('Y-m-d H:i:s')])
                                ->orderBy(['model_version' => SORT_DESC])
                                ->one();

        return static::getVersion($id, $eventStore->model_version);
    }

    /**
     * @return array<string|int, array<string,int|mixed>>
     */
    public function getChangesIn(int $version): array
    {
        return static::changesIn($this->getPrimaryKey(false), $version);
    }

    /**
     * @return array<string|int, array<string,int|mixed>>
     */
    public static function changesIn(int $id, int $version): array
    {
        $oldVersion = new static();
        if ($version > 1) {
            $oldVersion = static::getVersion($id, $version - 1);
        }

        /** @var EventStore|null $eventStore */
        $eventStore = EventStore::find()
                                ->where(['model_class' => static::class, 'model_id' => $id])
                                ->andWhere(['model_version' => $version])
                                ->one();

        if (empty($eventStore)) {
            return [];
        }

        $changes = [];
        if (is_array($eventStore->payload)) {
            foreach ($eventStore->payload as $attribute => $value) {
                $changes[$attribute] = [
                    'old' => $oldVersion->$attribute,
                    'new' => $value,
                ];
            }
        }

        return $changes;
    }

    public function getVersionNumber(): int
    {
        $primaryKey = $this->getPrimaryKey(false);

        codecept_debug('primaryKey:');
        codecept_debug($primaryKey);

        if ($this->isNewRecord || is_null($primaryKey)) {
            return 0;
        }

        if (!is_array($primaryKey)) {
            $vn = EventStore::find()->where([
                                                'model_class' => static::class,
                                                'model_id'    => $primaryKey,
                                            ])->max('model_version');

            return (int)$vn;
        }

        throw new NotSupportedException();
    }

    public function getEventStoreEntries()
    {
        return $this->hasMany(
            EventStore::class,
            ['model_id' => static::primaryKey()[0]]
        )->andOnCondition(['es_event_store.model_class' => static::class]);
    }

    /**
     * using Yii2-DI for Relations
     *
     * @param string                $class
     * @param array<string, string> $link
     * @param bool                  $multiple
     * @return \yii\db\ActiveQuery|\yii\db\ActiveQueryInterface
     */
    protected function createRelationQuery($class, $link, $multiple)
    {
        if (isset(Yii::$container->definitions[$class])) {
            $definition = Yii::$container->definitions[$class];
            if (is_array($definition) && isset($definition['class'])) {
                $class = $definition['class'];
            }
        }

        return parent::createRelationQuery($class, $link, $multiple); // TODO: Change the autogenerated stub
    }

}