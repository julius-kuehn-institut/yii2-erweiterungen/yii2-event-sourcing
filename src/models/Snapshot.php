<?php

namespace jki\es\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "es_snapshot".
 *
 * @property int $id
 * @property string|null $model_class
 * @property int|null $model_id
 * @property int|null $model_version
 * @property string|null $data
 * @property string|null $created_at
 */
class Snapshot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdocs
     * @param array<string|int, string|array<string|int,string>> $config
     */
    public final function __construct($config = []) {
        parent::__construct($config);
    }


    /**
     * @inheritdocs
     * @return array<string|int, mixed>
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['timestamp'] = [
            'class' => TimestampBehavior::class,
            'createdAtAttribute' => 'created_at',
            'updatedAtAttribute' => false,
            'value'              => new Expression('now()'),
        ];

        return $behaviors;
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'es_snapshot';
    }

    /**
     * {@inheritdoc}
     * @return array<string|int, mixed>
     */
    public function rules()
    {
        return [
            [['model_id', 'model_version'], 'default', 'value' => null],
            [['model_id', 'model_version'], 'integer'],
            [['data'], 'string'],
            [['model_class'], 'string', 'max' => 255],
            [['created_at'], 'safe'],
        ];
    }

}
