<?php

namespace jki\es\components;

use jki\es\events\DefaultEvent;
use jki\es\models\ActiveRecord;
use yii\db\Connection;

/**
 * @property Connection $db
 */
class Migration extends \yii\db\Migration
{
    public ?string $activeEsRecord = null;

    public function setActiveEsRecord(string $activeEsRecord): void
    {
        if(!class_exists($activeEsRecord)){
            throw new \Exception('Class '.$activeEsRecord.' not found');
        }
        if(!is_a($activeEsRecord, ActiveRecord::class, true)){
            throw new \Exception('Class '.$activeEsRecord.' must be instance of ' . ActiveRecord::class);
        }
        $this->activeEsRecord = $activeEsRecord;
    }

    public function disableEsRecord():void
    {
        $this->activeEsRecord = null;
    }

    /**
     * @inheritdocs
     * @param array<string, string|integer> $columns
     * @return void
     */
    public function insert($table, $columns)
    {
        parent::insert($table, $columns);
        if(!empty($this->activeEsRecord) && $this->activeEsRecord::tableName() === $table){
            /** @var ActiveRecord $model */
            $model = new $this->activeEsRecord;
            $model = $model::findOne($this->db->lastInsertID);
            DefaultEvent::onSave($model, true, $model->attributes);
        }
    }
    /**
     * @inheritdocs
     * @param array<string, string|integer> $columns
     * @param string|array<string|integer, string> $condition
     * @param array<string|integer, string> $params
     * @return void
     */
    public function update($table, $columns, $condition = '', $params = [])
    {
        $models = [];
        $writeEventStore = false;
        if(!empty($this->activeEsRecord) && $this->activeEsRecord::tableName() === $table) {
            /** @var ActiveRecord[] $models */
            $models = $this->activeEsRecord::find()->where($condition)->all();
            if(!empty($models)){
                $writeEventStore = true;
            }
        }
        parent::update($table, $columns, $condition, $params);
        if($writeEventStore){
            foreach ($models as $model){
                $changedAttributes = $model->getAttributes(array_keys($columns));
                $model->refresh();
                DefaultEvent::onSave($model, false, $changedAttributes);
            }
        }
    }

    /**
     * @inheritdocs
     * @param array<string|integer, string> $insertColumns
     * @param array<string|integer, string>|bool $updateColumns
     * @param array<string|integer, string> $params
     * @return void
     */
    public function upsert($table, $insertColumns, $updateColumns = true, $params = [])
    {
        if(!empty($this->activeEsRecord)) {
            throw new \Exception('Upsert not supported for Event Sourcing');
        }
        parent::upsert($table, $insertColumns, $updateColumns, $params);
    }


}