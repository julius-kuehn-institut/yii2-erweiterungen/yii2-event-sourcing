<?php

namespace jki\es\events;

use jki\es\models\ActiveRecord;
use yii\base\Event;

interface EventSourceEvent
{
    /**
     * @param array<string, string|integer>        $changedAttributes
     * @param array<string, string|integer>        $params
     */
    public static function onSave(ActiveRecord &$model, bool $insert, array $changedAttributes, array $params = []): bool;

    /**
     * @param array<string,string|integer|boolean>        $payload
     * @param array<string,string|integer|boolean>        $params
     */
    public static function onReplay(ActiveRecord &$model, array $payload, array $params = []): bool;
}