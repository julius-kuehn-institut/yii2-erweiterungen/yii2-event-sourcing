<?php

namespace jki\es\events;

use jki\es\models\ActiveRecord;
use jki\es\models\EventStore;

class DefaultEvent implements EventSourceEvent
{

    /**
     * @inheritdocs
     */
    public static function onSave(
        ActiveRecord &$model,
        bool         $insert,
        array        $changedAttributes,
        array        $params = []): bool
    {
        if ($insert) {
            return static::insert($model);
        }

        return static::update($model, $changedAttributes);
    }

    /**
     * @inheritdocs
     */
    static function onReplay(ActiveRecord &$model, array $payload, array $params = []): bool
    {
        $model->setAttributes($payload);
        return true;
    }

    /**
     * @param ActiveRecord $model
     * @return bool
     */
    protected static function insert(ActiveRecord $model): bool
    {
        $event                = new EventStore();
        $event->event         = self::getInsertEventName($model);
        $event->model_class   = $model::class;
        $event->model_id      = $model->getPrimaryKey();
        $event->model_version = 1;
        $event->payload       = $model->getAttributes();
        $event->save();
        return true;
    }

    /**
     * @param array<string, string|integer> $changedAttributes
     */
    protected static function update(ActiveRecord $model, array $changedAttributes): bool
    {
        $lastVersionNumber = EventStore::find()
                                       ->where(['model_class' => $model::class, 'model_id' => $model->getPrimaryKey()])
                                       ->max('model_version');
        $lastVersionNumber++;

        $event = new EventStore();
        //$event->event_id = $this->getEventId();
        $event->event         = self::getUpdateEventName($model);
        $event->model_class   = $model::class;
        $event->model_id      = $model->getPrimaryKey();
        $event->model_version = $lastVersionNumber;
        $event->payload       = $model->getAttributes(array_keys($changedAttributes));
        if ($event->save()) {
            if ($lastVersionNumber % $model->snapshotInterval == 0) {
                return $model->createSnapshot($event->model_version);
            }
            return true;
        }
        // @codeCoverageIgnoreStart
        return false;
        // @codeCoverageIgnoreEnd
    }

    /**
     * @param ActiveRecord $model
     * @return string
     */
    protected static function getInsertEventName(ActiveRecord $model): string
    {
        if(self::class == static::class)
            return "Create" . ucfirst($model::tableName());
        return static::class;
    }

    /**
     * @param ActiveRecord $model
     * @return string
     */
    protected static function getUpdateEventName(ActiveRecord $model): string
    {
        if (self::class == static::class)
            return "Update" . ucfirst($model::tableName());
        return static::class;
    }
}