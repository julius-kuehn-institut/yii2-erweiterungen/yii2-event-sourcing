<?php
namespace jki\es\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%es_snapshot}}`.
 */
class m230104_134512_create_es_snapshot_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%es_snapshot}}', [
            'id' => $this->bigPrimaryKey(),
            'model_class' => $this->string(),
            'model_id' => $this->integer(),
            'model_version' => $this->integer(),
            'data' => $this->text(),
            'created_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%es_snapshot}}');
    }
}
