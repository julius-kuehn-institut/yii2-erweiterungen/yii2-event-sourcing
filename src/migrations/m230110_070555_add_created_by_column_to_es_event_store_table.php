<?php

namespace jki\es\migrations;

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%es_event_store}}`.
 */
class m230110_070555_add_created_by_column_to_es_event_store_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%es_event_store}}', 'created_by', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%es_event_store}}', 'created_by');
    }
}
