<?php

namespace jki\es\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%es_event_store}}`.
 */
class m230104_123719_create_es_event_store_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%es_event_store}}', [
            'id' => $this->bigPrimaryKey(),
            'event_id' => $this->char(36),
            'event' => $this->string(),
            'model_class' => $this->string(),
            'model_id' => $this->integer(),
            'model_version' => $this->integer(),
            'payload' => $this->json(),
            'created_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%es_event_store}}');
    }
}
