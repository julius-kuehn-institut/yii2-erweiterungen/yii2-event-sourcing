<?php

namespace jki\es\gii;

use jki\es\models\ActiveRecord;
use yii\gii\generators\model\Generator;

class ConfigHelper
{
    /**
     * @param array<string, string> $config Your model generator configuration will replace the default configuration.
     *                                      For more Details:
     * @return array<string, string>
     * @see \yii\gii\generators\model\Generator
     */
    public static function model(array $config = []): array
    {
        return array_merge(
            [
                'class'     => Generator::class,
                'baseClass' => ActiveRecord::class,
            ],
            $config
        );
    }
}