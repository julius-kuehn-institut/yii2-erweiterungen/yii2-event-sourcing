<?php

declare(strict_types=1);

namespace tests\Support;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause($vars = [])
 *
 * @SuppressWarnings(PHPMD)
*/
class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

    public function runPrivateMethode(Object $object, string $methodeName, array $arguments = [])
    {
        $reflectionClass = new \ReflectionClass($object);
        $method          = $reflectionClass->getMethod($methodeName);
        $method->setAccessible(true);
        return $method->invoke($object, $arguments);
    }

    public function setPrivateProperty(Object $object, string $name, $value){
        $reflectionClass = new \ReflectionClass($object);
        $property = $reflectionClass->getProperty($name);
        $property->setAccessible(true);
        $property->setValue($object, $value);
    }

    public function getPrivateProperty(Object $object, string $name){
        $reflectionClass = new \ReflectionClass($object);
        $property = $reflectionClass->getProperty($name);
        $property->setAccessible(true);
        return $property->getValue($object);
    }
}
