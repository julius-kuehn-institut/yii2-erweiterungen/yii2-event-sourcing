<?php

namespace tests\_app\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%testmodel}}`.
 */
class m230109_123518_create_testmodel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%testmodel}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'strasse' => $this->string(),
            'plz' => $this->string(5),
            'ort' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%testmodel}}');
    }
}
