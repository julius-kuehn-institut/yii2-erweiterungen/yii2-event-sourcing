<?php


use tests\_app\models\User;

return [
    'id'                  => 'yii2-event-sourcing-tests',
    'controllerNamespace' => 'tests\\_app\\controllers',
    'basePath'            => dirname(__DIR__),
    'aliases'             => [
        '@tests'   => dirname(__DIR__, 2),
        '@vendor'  => dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'vendor',
        '@bower'   => '@vendor/bower-asset',
        '@npm'     => '@vendor/npm-asset',
    ],
    'components'          => [
        'user'         => [
            'identityClass'   => User::class
        ],
        'assetManager' => [
            'basePath' => dirname(__DIR__) . '/web/assets',
        ],
        'db'           => file_exists(__DIR__ . '/db.php') ? require __DIR__ . '/db.php' : [],
        'urlManager'   => [
            'showScriptName' => true,
        ],
//        'request'      => [
//            'cookieValidationKey'  => 'test',
//            'enableCsrfValidation' => false,
//        ],
        'i18n'         => [
            'translations' => [
            ],
        ],
    ],
    'params'              => [],
];
