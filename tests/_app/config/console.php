<?php


use yii\console\controllers\MigrateController;

return [
    'id'                  => 'yii2-event-sourcing-tests',
    'controllerNamespace' => 'tests\\_app\\controllers',
    'basePath'            => dirname(__DIR__),
    'aliases'             => [
        '@jki/es'  => dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'src',
        '@tests'   => dirname(__DIR__, 2),
        '@vendor'  => dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'vendor',
        '@bower'   => '@vendor/bower-asset',
        '@npm'     => '@vendor/npm-asset',
    ],
    'components'          => [
//        'user'         => [
//            'identityClass'   => User::class
//        ],
        'assetManager' => [
            'basePath' => dirname(__DIR__) . '/web/assets',
        ],
        'db'           => file_exists(__DIR__ . '/db.php') ? require __DIR__ . '/db.php' : [],
        'urlManager'   => [
            'showScriptName' => true,
        ],
//        'request'      => [
//            'cookieValidationKey'  => 'test',
//            'enableCsrfValidation' => false,
//        ],
        'i18n'         => [
            'translations' => [
            ],
        ],
    ],
    'params'              => [],
    'controllerMap'       => [
        'migrate' => [
            'class'               => MigrateController::class,
            'migrationPath'       => [],
            'migrationNamespaces' => [
                'tests\\_app\\migrations',
                'jki\\es\\migrations',
            ],
        ],
        'drone' => [
            'class'        => \jki\droneTrSupport\DroneController::class,
            'token'        => '63bd1953495c263bd1953495c3',
            'outputFolder' => dirname(__DIR__,3) . '/tests/_output'
        ]
    ],
];
