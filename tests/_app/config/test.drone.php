<?php

$db = require __DIR__ . '/db.drone.php';
$config = require __DIR__ . '/test.php';

$config['components']['db'] = $db;

return $config;