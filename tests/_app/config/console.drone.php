<?php

$db = require __DIR__ . '/db.drone.php';

$config = require __DIR__ . '/console.php';
$config['components']['db'] = $db;

return $config;
