<?php
return [
    'class'               => 'yii\db\Connection',
    'dsn'                 => "mysql:host=mysqldb;dbname=test_db",
    'username'            => 'test_user',
    'password'            => 'test_pass',
    'charset'             => 'utf8',

];