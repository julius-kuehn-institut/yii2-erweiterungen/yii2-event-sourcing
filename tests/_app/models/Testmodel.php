<?php

namespace tests\_app\models;


use jki\es\events\DefaultEvent;
use jki\es\models\ActiveRecord;
use tests\_app\events\Anmeldung;
use tests\_app\events\Umzug;

/**
 * @property string name
 * @property string email
 * @property string strasse
 * @property string plz
 * @property string ort
 */
class Testmodel extends ActiveRecord
{
    const SCENARIO_UMZUG = 'umzug';
    const SCENARIO_ANMELDUNG = 'anmeldung';

    public int $snapshotInterval = 3;

    protected array $eventList =[
        self::SCENARIO_DEFAULT => DefaultEvent::class,
        self::SCENARIO_UMZUG => Umzug::class,
        self::SCENARIO_ANMELDUNG => Anmeldung::class,
    ];

    public static function tableName()
    {
        return "testmodel";
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['test'] = ['name', 'email', 'strasse', 'plz', 'ort'];
        $scenarios[static::SCENARIO_UMZUG] = ['name', 'email', 'strasse', 'plz', 'ort'];
        $scenarios[static::SCENARIO_ANMELDUNG] = ['name', 'email', 'strasse', 'plz', 'ort'];
        return $scenarios;
    }

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'required'],
            [['name','email','strasse','ort'], 'string', 'max' => 255],
            [['plz'], 'string', 'max' => 5],
        ];
    }

}