<?php

namespace Unit\components;

use jki\es\models\EventStore;
use tests\_app\models\User;
use jki\es\components\Migration;
use jki\es\models\ActiveRecord;
use tests\_app\models\Testmodel;

class MigrationTest extends \Codeception\Test\Unit
{
    /**
     * @var \tests\Support\UnitTester
     */
    public $tester;

    public function testSetActiveEsRecord()
    {
        $migration = new Migration();

        //Test Errors
        try {
            $migration->setActiveEsRecord('app\models\NotExistingModel');
        } catch (\Exception $e) {
            $this->assertStringContainsString('not found', $e->getMessage());
        }

        try {
            $migration->setActiveEsRecord(User::class);
        } catch (\Exception $e) {
            $this->assertStringContainsString('must be instance of ' . ActiveRecord::class, $e->getMessage());
        }

        $migration->setActiveEsRecord(Testmodel::class);
        $this->assertEquals(
            Testmodel::class,
            $this->tester->getPrivateProperty($migration, 'activeEsRecord')
        );

        $migration->disableEsRecord();

        $this->assertNull(
            $this->tester->getPrivateProperty($migration, 'activeEsRecord')
        );
    }

    public function testInsert()
    {
        $migration = new Migration();
        $migration->setActiveEsRecord(Testmodel::class);
        $migration->insert('testmodel', [
            'name'    => 'Maxi Musterfrau',
            'email'   => 'maxi.musterfrau@example.com',
            'strasse' => 'Musterstraße 2',
            'plz'     => '12345',
            'ort'     => 'Musterstadt'
        ]);

        $model = $this->tester->grabRecord(
            Testmodel::class,
            [
                'name'    => 'Maxi Musterfrau',
                'email'   => 'maxi.musterfrau@example.com'
            ]
        );

        $this->tester->seeRecord(
            \jki\es\models\EventStore::class,
            [
                'event'         => 'CreateTestmodel',
                'model_class'   => Testmodel::class,
                'model_id'      => $model->id,
                'model_version' => 1,
            ]
        );
    }

    public function testUpdate()
    {
        $migration = new Migration();
        $migration->setActiveEsRecord(Testmodel::class);
        $migration->update('testmodel',
            [
                'strasse' => 'Musterweg 3',
                'plz' => '54321',
                'ort' => 'Musterdorf'
            ],
            [
            'name'    => 'Maxi Musterfrau',
            'email'   => 'maxi.musterfrau@example.com'
            ]
        );

        /** @var Testmodel $model */
        $model = $this->tester->grabRecord(
            Testmodel::class,
            [
                'name'    => 'Maxi Musterfrau',
                'email'   => 'maxi.musterfrau@example.com'
            ]
        );

        $this->assertEquals('Musterweg 3', $model->strasse);
        $this->assertEquals('54321', $model->plz);
        $this->assertEquals('Musterdorf', $model->ort);

        $this->tester->seeRecord(
            EventStore::class,
            [
                'event'         => 'UpdateTestmodel',
                'model_class'   => Testmodel::class,
                'model_id'      => $model->id,
                'model_version' => 2,
            ]
        );

        $this->assertEquals(
            [
                'strasse' => ['old' => 'Musterstraße 2', 'new' => 'Musterweg 3'],
                'plz' => ['old' => '12345', 'new' => '54321'],
                'ort' => ['old' => 'Musterstadt', 'new' => 'Musterdorf']
            ],
            $model->getChangesIn(2)
        );
    }

    public function testUpsert()
    {
        $migration = new Migration();
        $migration->setActiveEsRecord(Testmodel::class);
        try {
            $migration->upsert('testmodel', [
                'id'    => 1,
                'name'  => 'Max Mustermann',
                'email' => 'max.mustermann@example.com'
            ]);
        } catch (\Exception $e){
            $this->assertStringContainsString('Upsert not supported for Event Sourcing', $e->getMessage());
        }
        $migration->disableEsRecord();

        //It works, but you shouldn't do that with event-sourced tables
        $migration->upsert('testmodel', [
            'id'    => 1,
            'name'  => 'Max Mustermann',
            'email' => 'max.mustermann@example.com'
        ]);
        $this->tester->seeRecord(Testmodel::class, [
            'id'    => 1,
            'name'  => 'Max Mustermann',
            ]
        );
    }
}
