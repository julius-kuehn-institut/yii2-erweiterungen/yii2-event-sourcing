<?php

namespace tests\Unit\models;

use Codeception\Test\Unit;
use jki\es\models\EventStore;
use jki\es\models\Snapshot;
use tests\_app\events\Umzug;
use tests\_app\models\Testmodel;
use Tests\Support\UnitTester;
use function PHPUnit\Framework\assertEquals;

class TestModelTest extends Unit
{
    /** @var UnitTester */
    public $tester;

    private $model;

    public function testInsertUpdate()
    {
        $model = new \tests\_app\models\Testmodel();
        $this->assertEquals(0, $model->versionNumber);
        $model->setAttributes([
                                  'name'    => 'Max Mustermann',
                                  'email'   => 'max.mustermann@example.com',
                                  'strasse' => 'Musterstraße 1',
                                  'plz'     => '12345',
                                  'ort'     => 'Musterstadt',
                              ]);
        $this->assertTrue($model->save());

        $this->model = $model;

        $this->tester->seeRecord(
            EventStore::class,
            [
                'event'         => 'CreateTestmodel',
                'model_class'   => $model::class,
                'model_id'      => $model->id,
                'model_version' => 1,
            ]
        );

        $this->assertEquals(1, $model->getVersionNumber());

        $changes = $model->getChangesIn(1);

        foreach ($changes as $attribute => $change) {
            $this->assertNull($change['old']);
            $this->assertEquals($model->$attribute, $change['new']);
        }

        $model->name = 'Max Mustermann 2';
        $this->assertTrue($model->save());
        $this->assertEquals(2, $model->versionNumber);


        $this->tester->assertEquals(
            [
                'name' =>
                    [
                        'old' => 'Max Mustermann',
                        'new' => 'Max Mustermann 2'
                    ]
            ],
            $model->getChangesIn(2)
        );

        sleep(2);
        $model->email = 'max.mustermann2@example.com';
        $model->save();
        $this->assertEquals(3, $model->versionNumber);
        $timestamp = time();
        sleep(2);

        $snapshot = $this->tester->grabRecord(
            Snapshot::class,
            [
                'model_class' => $model::class,
                'model_id' => $model->id,
                'model_version' => 3
            ]
        );

        $this->assertEquals($model->attributes, unserialize($snapshot->data));

        $model->plz = '54321';
        $model->save();

        $this->assertTrue($model->createSnapshot());

        $this->tester->seeRecord(
            Snapshot::class,
            [
                'model_class' => $model::class,
                'model_id' => $model->id,
                'model_version' => 4
            ]
        );

        $oldModel = $model::getVersion($model->id, 3);
        $this->assertEquals('12345', $oldModel->plz);

        $olderModel = $model::getVersionAtTime($model->id, $timestamp);

        $this->assertTrue(is_a($olderModel, $model::class));
        $this->assertEquals('12345', $olderModel->plz);
        $this->assertEquals('Max Mustermann 2', $olderModel->name);
        $this->assertEquals('max.mustermann2@example.com', $olderModel->email);

        $sameOlderModel = $model::getVersionAtTime($model->id, date('Y-m-d H:i:s', $timestamp));
        $this->assertEquals($olderModel->attributes, $sameOlderModel->attributes);

        $sameOlderModel = $model::getVersionAtTime($model->id, \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', $timestamp)));
        $this->assertEquals($olderModel->attributes, $sameOlderModel->attributes);

        $this->assertEquals([], $model->getChangesIn(10000));
    }


    public function testNotSupportedScenario()
    {
        $model = new \tests\_app\models\Testmodel();
        $model->setScenario('test');
        $model->setAttributes(['name'    => 'Max Mustermann']);
        $this->assertFalse($model->save());
    }

    public function testError(){
        $model = new \tests\_app\models\Testmodel();
        $model->setAttributes(
            [
                'name'    => 'Max Mustermann',
                'plz'   => '123456',
            ]
        );
        $this->assertFalse($model->save());
    }

    public function testEvents()
    {
        $model = new \tests\_app\models\Testmodel();
        $model->setScenario(Testmodel::SCENARIO_ANMELDUNG);
        $model->setAttributes(
            [
                'name'    => 'Erika Mustermann',
                'email'   => 'erika.mustermann@example.com',
                'strasse' => '123 Fake Street',
                'plz'     => '04711',
                'ort'     => 'Fake City',
            ]
        );
        $model->save();
        $model->setScenario(Testmodel::SCENARIO_UMZUG);
        $model->setAttributes(
            [
                'strasse' => 'Beispielgasse 123',
                'plz'     => '12345',
                'ort'     => 'Beispielstadt',
            ]
        );
        $model->save();

        $this->tester->seeRecord(EventStore::class, [
            'event' => Umzug::class,
            'model_class' => $model::class,
            'model_id' => $model->id,
            'model_version' => 2
        ]);

        $modelReplay = $model::getVersion($model->id, 2);

        $this->assertEquals($model->attributes, $modelReplay->attributes);

    }


    public function testReplay()
    {
        $expected_attributes_v3 = json_decode(
            file_get_contents(
                codecept_data_dir() . '/testmodel_2_v3_attributes.json'
            ),
            true
        );
        $model_vers_3_sn = \tests\_app\models\Testmodel::getVersion(2, 3);

        $this->assertEquals($expected_attributes_v3, $model_vers_3_sn->attributes);

        Snapshot::deleteAll(['model_id' => 2, 'model_version' => 3, 'model_class' => \tests\_app\models\Testmodel::class]);

        $model_vers_3_rb = \tests\_app\models\Testmodel::getVersion(2, 3);

        $this->assertEquals($expected_attributes_v3, $model_vers_3_rb->attributes);
    }
}


