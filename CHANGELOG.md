<!--- BEGIN HEADER -->
# Changelog Yii2 Event Sourcing

All notable changes to this project will be documented in this file. test
<!--- END HEADER -->

## [0.3.0](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/compare/v0.2.1...v0.3.0) (31.03.2023)

### Features

* AR changesIn as static Function ([d1d333](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/d1d3333d82bfab8c6a66e39a904bf516255cf6c9))


---

## [0.2.1](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/compare/v0.2.0...v0.2.1) (29.03.2023)

### Bug Fixes

* False ID-Value for model-version 0 ([be3285](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/be3285f9cc72b725f32033745729ad21802f9dc8))


---

## [0.2.0](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/compare/v0.1.1...v0.2.0) (29.03.2023)

### Features

* Add afterReplay method and Event ([20bac8](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/20bac87407ad1c35984e47925734767367348467))
* VersionsNumber ([c7306f](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/c7306f1f68284e8b4e2d693df78a7917a7d5e054))

### Bug Fixes

* Data-problem with PostgreSQL ([dc869f](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/dc869f1b819f91822aba06160b003b367c56fcbd))

### Documentation

* Add missing created_by property to phpdocs ([711682](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/7116822fdb3374858f8a5ebe706976ff7649f518))


---

## [0.1.1](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/compare/v0.1.0...v0.1.1) (03.03.2023)

### Features

* Gii-Model Configuration ([1c8fdd](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/1c8fddb12cd37b4c3368513a995f735079d167c0))
* Migration-Support (#4) ([efd18a](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/efd18aedf68825cec443cb07c20d9dae7e848bc7))
* Replay ([217895](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/2178954c36fcf52f7db154b5092557b2d8961c72))

### Bug Fixes

* Problem create empty Change-Records ([9fab81](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/9fab813d2c6c6f4bf2d3da55ffb8b1da2950fe65))

### Code Refactoring

* Php-stan ([5cba8a](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/5cba8a351f7af3e609494a04d93925dbb7dac92e))


---

## [0.1.0](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/compare/0.0.0...v0.1.0) (10.01.2023)

### Features

* Versionierung ([85aa30](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/85aa307792d02f4db38b15189f61d755a04b8c42))

### Documentation

* New Changelog ([4950bb](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/4950bb04d71a9a1ba3679e69ed752da38ac4ceee), [6fd5d9](https://gitea.julius-kuehn.de/toni.schreiber/yii2-event-sourcing/commit/6fd5d93c4a50a4527e12cd78274921c5616ac404))


---

